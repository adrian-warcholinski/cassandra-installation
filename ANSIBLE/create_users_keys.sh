for i in `cat idemia_users.list`
do
echo "=========> Processing key for $i =========="
str1=`cat ssh_keys.yaml | grep -A2 $i | grep type | awk '{print $2}' | awk -F "'" '{print $2}'`
str2=`cat ssh_keys.yaml | grep -A2 $i | grep key | awk '{print $2}' | awk -F "\"" '{print $2}'`
key_str="$str1 $str2"
echo $key_str > ~/ANSIBLE/roles/os/create_users/templates/$i.key
cat ~/ANSIBLE/roles/os/create_users/templates/$i.key
done
