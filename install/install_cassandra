#!/bin/bash

CASSANDRA_REPO="311x"
JMX_EXPORTER_VERSION=0.12.0
SJK_VERSION=0.17

# Cassandra repository configuration
echo "deb https://downloads.apache.org/cassandra/debian ${CASSANDRA_REPO} main" | tee /etc/apt/sources.list.d/cassandra.sources.list

# Reaper repository configuration
echo "deb https://dl.bintray.com/thelastpickle/reaper-deb wheezy main" | tee /etc/apt/sources.list.d/reaper.sources.list

# Medusa repository configuration
echo "deb https://dl.bintray.com/thelastpickle/medusa-deb bionic main" | tee /etc/apt/sources.list.d/medusa.sources.list

# repository key for Cassandra
curl https://downloads.apache.org/cassandra/KEYS | apt-key add -

# repository key for Reaper & Medusa
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 2895100917357435

apt-get update
apt-get -y upgrade
apt-get install -y --no-install-recommends --reinstall \
             lsb-base procps sysvinit-utils ca-certificates curl \
             libjemalloc1 software-properties-common dirmngr \
             gpg-agent apt-transport-https rsync \
             ethtool net-tools sysstat pciutils ntp ntpstat numactl \
             lvm2 curl lsof iproute2 dmidecode smartmontools \
             ncurses-term dstat htop less nmon nano strace iperf \
             openjdk-8-jre-headless cassandra reaper cassandra-medusa

systemctl stop cassandra
systemctl disable cassandra

rm -rf /var/lib/cassandra/*

mkdir /usr/local/jmx_exporter
curl -L "https://search.maven.org/remotecontent?filepath=io/prometheus/jmx/jmx_prometheus_javaagent/${JMX_EXPORTER_VERSION}/jmx_prometheus_javaagent-${JMX_EXPORTER_VERSION}.jar" > /usr/local/jmx_exporter/jmx_prometheus_javaagent.jar
chmod 755 /usr/local/jmx_exporter

mkdir /usr/local/sjk
curl -L "https://search.maven.org/remotecontent?filepath=org/gridkit/jvmtool/sjk-plus/${SJK_VERSION}/sjk-plus-${SJK_VERSION}.jar" > /usr/local/sjk/sjk.jar
chmod 755 /usr/local/sjk
echo 'java -jar /usr/local/sjk/sjk.jar $@'> /usr/local/bin/sjk
chmod 700 /usr/local/bin/sjk

# https://docs.datastax.com/en/cassandra-oss/3.0/cassandra/install/installRecommendSettings.html
echo "net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.core.rmem_default = 16777216
net.core.wmem_default = 16777216
net.core.optmem_max = 40960
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 65536 16777216" | sudo tee -a /etc/sysctl.d/cassandra.conf > /dev/null

sudo sysctl -p /etc/sysctl.d/cassandra.conf

echo "# Provided by the cassandra package
cassandra  -  memlock  unlimited
cassandra  -  nofile   100000
cassandra  -  as       unlimited
cassandra  -  nproc    32768" | sudo tee /etc/security/limits.d/cassandra.conf > /dev/null

sudo sysctl -p

echo never | sudo tee /sys/kernel/mm/transparent_hugepage/defrag > /dev/null
